#!/bin/bash

git pull
gradle clean build buildDocker -x test
LATEST_IMAGE=`docker images --format "{{.Repository}}:{{.Tag}}" | grep "ecsh/homepage" | head -1`
docker tag $LATEST_IMAGE ecsh/homepage:latest
echo "deploying $LATEST_IMAGE"
exec sudo systemctl restart homepage 
