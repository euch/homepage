-- Table: public.fias_event_log

-- DROP TABLE public.fias_event_log;

CREATE TABLE public.fias_event_log
(
  id bigint NOT NULL,
  add_date timestamp without time zone NOT NULL,
  finish_date timestamp without time zone,
  params character varying(255),
  result character varying(255),
  type character varying(255) NOT NULL,
  CONSTRAINT fias_event_log_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.fias_event_log
  OWNER TO postgres;
