ALTER TABLE news drop column if exists pub_date;
ALTER TABLE news ADD COLUMN pub_date TIMESTAMP;