
ALTER TABLE public.fias_event_log add column successful boolean;
ALTER TABLE public.fias_event_log ALTER COLUMN successful SET NOT NULL;
