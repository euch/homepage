<#macro basicLayout>
    <#assign security=JspTaglibs["/META-INF/security.tld"] />
    <#setting locale="ru_RU">

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="/css/normalize.css"/>
    <link rel="stylesheet" href="/css/main.css"/>
    <link rel="stylesheet" href="/lib/semantic-ui/semantic.min.css"/>

    <script src="/js/vendor/jquery-1.11.3.min.js"></script>
    <script src="/lib/semantic-ui/semantic.min.js"></script>

</head>
<body>
<div class="wrapper">
    <@security.authorize access="isAuthenticated()">
        <nav class="ui fixed menu inverted navbar">
            <a href="/logout" class="active right item">Log out</a>
        </nav>
    </@security.authorize>

    <div class="ui one column doubling grid container">
        <div class="column">
            <#nested/>
        </div>
    </div>

    <div class="push"></div>
</div>


</body>

</html>
</#macro>