<#import "/layout/basic.ftl" as layout>
<@layout.basicLayout>
<#assign security=JspTaglibs["/META-INF/security.tld"] />

<script>


 $(document).ready(function (event) {
   loadStats();
   setInterval(loadStats, 10000);
 });

 var loadStats = function() {
    loadStat('crawlerInfo');
    loadStat('proxyInfo');
    loadStat('twitter2rssInfo');
    loadStat('openttdInfo');
    loadStat('torrentBoxInfo');
    loadStat('fiasInfo');
 }

 var loadStat = function (infoType) {
   var xhr = new XMLHttpRequest();
      xhr.open('GET', '/mon/'+infoType, true);
      xhr.timeout = 30000;
      xhr.onreadystatechange = function () {
        if (4 === xhr.readyState && 200 === xhr.status) {
          if (xhr.responseText.length > 0) {
            $('#'+infoType+'Tr').removeClass('negative').addClass('positive');
            $('#'+infoType+'Status').html('<i class="icon checkmark"></i> Up');
            $('#'+infoType+'Notes').html(xhr.responseText);
          } else {
            $('#'+infoType+'Tr').removeClass('positive').addClass('negative');
            $('#'+infoType+'Status').html('<i class="icon minus"></i> Down');
            $('#'+infoType+'Notes').html('');
          }
        }
      }
      xhr.send();
 }
</script>

<div class="ui message">
  <div class="header">
    Мониторинг поделок
  </div>
  <p>На этой странице нормальным людям может быть интересен только статус <a href="http://openttd.org">OpenTTD</a>-сервера <a href="${ttdServerUrl}">${ttdServerName}</a></p>
</div>

<table class="ui celled table">
  <thead>
    <tr>
      <th>Сервис</th>
      <th>Статус</th>
      <th>Дополнительная инфа</th>
    </tr>
  </thead>
  <tbody>
      <tr id="crawlerInfoTr">
        <td>RSS Crawler<br><a href="/view/news">address</a></td>
        <td id="crawlerInfoStatus"><div class="ui active small inline loader"></div></td>
        <td id="crawlerInfoNotes"><div class="ui active small inline loader"></div></td>
      </tr>
      <tr id="twitter2rssInfoTr">
        <td>Twitter-RSS gate</td>
        <td id="twitter2rssInfoStatus"><div class="ui active small inline loader"></div></td>
        <td></td>
      </tr>
      <tr id="proxyInfoTr">
        <td>Proxy</td>
        <td id="proxyInfoStatus"><div class="ui active small inline loader"></div></td>
        <td></td>
      </tr>
      <tr id="openttdInfoTr">
        <td>OpenTTD:<br><a href="${ttdServerUrl}">${ttdServerName}</a></td>
        <td id="openttdInfoStatus"><div class="ui active small inline loader"></div></td>
        <td id="openttdInfoNotes"><div class="ui active small inline loader"></div></td>
      </tr>
      <tr id="torrentBoxInfoTr">
        <td>TorrentBox
        <@security.authorize access="hasRole('ADMIN')">
            <br>
            <a href="${torrentboxUrl}">address</a>
        </@security.authorize>
        </td>
        <td id="torrentBoxInfoStatus"><div class="ui active small inline loader"></div></td>
        <td></td>
      </tr>
      <tr id="fiasInfoTr">
        <td>FIAS API<br><a href="/view/api">address</a></td>
        <td id="fiasInfoStatus"><div class="ui active small inline loader"></div></td>
        <td id="fiasInfoNotes"><div class="ui active small inline loader"></div></td>
      </tr>
  </tbody>

</table>

</@layout.basicLayout>