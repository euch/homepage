<#import "layout/basic.ftl" as layout>
<@layout.basicLayout>

<script>
    $(document).ready(function () {
        $('.ui.form')
                .form({
                    fields: {
                        username: {
                            identifier: 'username',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please enter your username'
                                }
                            ]
                        },
                        password: {
                            identifier: 'password',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please enter your password'
                                }
                            ]
                        }
                    }
                })
        ;
    })
</script>
<div>

    <div class="ui middle aligned center aligned grid">

        <div class="row">

            <div class="six wide column">
                <h3 class="ui teal image header">
                    <div class="content">
                        Sign in
                    </div>
                    <#if RequestParameters.error??>
                        <div class="content" style="color: red">
                            Invalid password or username
                        </div>
                    </#if>
                </h3>
                <form class="ui large form" action="/login" method="post">
                    <div class="ui stacked segment">
                        <div class="field">
                            <div class="ui left icon input">
                                <i class="user icon"></i>
                                <input type="text" name="username" placeholder="username"/>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui left icon input">
                                <i class="lock icon"></i>
                                <input type="password" name="password" placeholder="password"/>
                            </div>
                        </div>
                        <button type="submit" class="ui fluid large teal submit button">Login</button>
                    </div>

                    <div class="ui error message"></div>

                </form>
            </div>

        </div>

    </div>

</div>
</@layout.basicLayout>
