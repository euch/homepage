<#import "layout/basic.ftl" as layout>
<@layout.basicLayout>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['annotationchart']}]}"></script>

<script>
  var defaultView = 'stats';
  view = defaultView;

  $(document).ready(function () {
    loadView(defaultView);
    $('.demo.menu .item').tab({history: false});
  });

  var separateLink = function(viewPath) {
    return '<a href="'+viewPath+'" class="ui basic button"><i class="external icon"/>Открыть отдельно</a>';
  }

  var loadView = function (view) {
    var xhr = new XMLHttpRequest();
    var viewPath = "/view/" + view;
    xhr.open('GET', viewPath, true);
    xhr.timeout = 30000;
    xhr.onreadystatechange = function () {
      if (4 === xhr.readyState && 200 === xhr.status && xhr.responseText.length > 0) {
        $('#'+view).html(xhr.responseText + '<br/>' + separateLink(viewPath));
      }
    };
    xhr.send();
  };
</script>

<div class="ui three pointing demo menu">
    <a class="active item" data-tab="first" onclick="loadView('stats')">Мониторинг</a>
    <a class="item" data-tab="second"  onclick="loadView('news')">Новости</a>
    <#if fiasEnabled>
        <a class="item" data-tab="third" onclick="loadView('fias')">ФИАС API</a>
    <#else>
        <span class="item disabled">ФИАС API</span>
    </#if>
</div>
<div id="stats" class="ui active tab segment" data-tab="first"></div>
<div id="news" class="ui tab segment" data-tab="second"></div>
<div id="fias" class="ui tab segment" data-tab="third"></div>

</@layout.basicLayout>
