<#import "/layout/basic.ftl" as layout>
<@layout.basicLayout>
<#assign security=JspTaglibs["/META-INF/security.tld"] />


<script type="text/javascript">

 $(document).ready(function (event) {

 $('.ui.accordion')
       .accordion({
        exclusive: true
               })
     ;
    loadData();
    setInterval(loadData, 60000);
  });

  var target = '#tableData'

  var loadData = function () {
    var filter = document.getElementById('filter').value;
    var xhr = new XMLHttpRequest();
    if (filter === undefined || filter.length == 0) {
      xhr.open('GET', '/news', true);
    } else {
      xhr.open('GET', '/news/?filter=' + filter, true);
    }
    xhr.timeout = 30000;
    xhr.onreadystatechange = function () {
      if (4 === xhr.readyState && 200 === xhr.status && xhr.responseText.length > 0) {
          $(target).html(xhr.responseText)
    if (filter === undefined || filter.length == 0) {} else {
          $('#searchResultNotify').show(500)
          }
        }
    };
    xhr.send();
    };

  google.load("visualization", "1", {packages:["corechart"]});
      function drawChart(keywords) {
          var jsonData = $.ajax({
              url: "rest/news/charts/sources_by_keywords?keywords="+keywords,
              dataType: "json",
              async: false
          }).responseText;
          var data = google.visualization.arrayToDataTable(JSON.parse(jsonData));
          var view = new google.visualization.DataView(data);
          var options = {
              width: 900,
              height: 900,
              legend: { position: 'top', maxLines: 3 },
              bar: { groupWidth: '75%' },
              isStacked: true,
          };
          var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
          chart.draw(view, options);
      }
</script>

<div class="ui styled fluid accordion">
  <div class="title active">
    <i class="dropdown icon"></i>
    Свежие новости
  </div>
  <div class="content active">
    <div class="ui search" style="padding-bottom: 5px">
      <form onsubmit="loadData();return false;">
        <div class="ui input">
          <input id="filter" class="prompt" type="text" placeholder="Искать...">
        </div>
      </form>
    </div>

    <div id="tableData">
      <#include "newsContent.ftl"/>
    </div>
  </div>
  <div class="title">
    <i class="dropdown icon"></i>
    Статистика: Ключевые слова в зависимости от источника
  </div>
  <div class="content">
      <#include "charts.ftl"/>
  </div>
 <@security.authorize access="hasRole('ADMIN')">
  <div class="title">
      <i class="dropdown icon"></i>
      Добавление источников
    </div>
    <div class="content">
           <div class="ui three column grid">
             <div class="ui column form">
               <input id="rss_url" type="text" name="first-name" placeholder="http://">
               <button class="ui button" type="submit" onclick="addRss()">RSS</button>
             </div>
             <div class="ui column form">
               <input id="username" type="text" name="first-name" placeholder="@username">
               <button class="ui button" type="submit" onclick="addTwitterUsername()">Twitter Username</button>
             </div>
             <div class="ui column form">
               <input id="hashtag" type="text" name="first-name" placeholder="#tag">
               <button class="ui button" type="submit" onclick="addTwitterHashtag()">Twitter Tag</button>
             </div>
           </div>
    </div>
     </@security.authorize>
</div>


<@security.authorize access="hasRole('ADMIN')">


 <script>
   var addRss = function () {
     var url = document.getElementById('rss_url');
     var xhr = new XMLHttpRequest();
     xhr.open('POST', '/news/?url='+url.value, true);
     xhr.send();
     url.value = ""
   };
   var addTwitterUsername = function () {
     var user = document.getElementById('username');
     var xhr = new XMLHttpRequest();
     xhr.open('POST', '/news/twi-user/?username='+user.value, true);
     xhr.send();
     user.value = ""
   };
   var addTwitterHashtag = function () {
     var tag = document.getElementById('hashtag');
     var xhr = new XMLHttpRequest();
     xhr.open('POST', '/news/twi-tag/?hashtag='+tag.value, true);
     xhr.send();
     tag.value = ""
   };
 </script>
 </@security.authorize>

 </@layout.basicLayout>