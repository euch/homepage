<script>
 $(document).ready(function (event) {

 $('.newsTitle').popup({
         inline: true
     });
     });
     </script>

    <#if newsList??>
<div id="searchResultNotify" class="ui positive message" style="display:none;">
  <#else>
<div id="searchResultNotify" class="ui negative message" style="display:none;">
</#if>
      <i class="close icon" onclick="$('#searchResultNotify').hide()"></i>
      <div class="header">
    <#if newsList??>
  Нашлось ${newsList?size} новостей!
  <#else>
  Не найдено ни одной новости
</#if>

      </div>
    </div>
<table class="ui large celled table">
    <thead>
    <tr>
        <th>Заголовок</th>
        <th>Дата сохранения (UTC)</th>
        <th>Дата публикации (RAW)</th>
        <th>Описание</th>
    </tr>
    </thead>
<#if newsList??>
    <tbody>
        <#list newsList as news>
        <tr>
            <td>${news.newsSource.name}</td>
            <td>${(news.saveDate?string)}</td>
            <#if news.pubDate??>
              <td>${(news.pubDate?string)}</td>
            <#else>
              <td></td>
            </#if>
            <td><a class="newsTitle" href="${news.link}" target="_blank" >${news.title}</a>
                <div class="ui flowing popup top left transition hidden">
                    ${(news.content!news.title)}
                </div>
            </td>
        </tr>
        </#list>
    </tbody>
</#if>

</table>

<div class="ui message">
  <p>Заголовки новостей, собранных из открытых источников по RSS, а также твиты отображаются здесь "как есть". Оригиналы доступны по ссылкам.</p>
  <p>Эти записи зеркалируются сюда для удобного быстрого просмотра и последующего анализа.</p>
</div>