<#import "/layout/basic.ftl" as layout>
<@layout.basicLayout>

<h4 class="ui header">Уточнение адреса</h4>
<p>Возврат адреса из ФИАС, тождественного переданному в JSON</p>
<p>Адресные объекты включают в себя все адресные единицы уровнем от региона до улицы</p>
<p>Элементы адреса в запросе должны быть в процессе увеличения (например, улица-город-регион)</p>
<p>Префиксы адресных единиц (улица, бульвар, пгт и пр.) в текущей реализации игнорируются</p>
<p><a href="/rest/fias_clarify?address_json=[&quot;Рябиновый&quot;,&quot;Тольятти&quot;,&quot;Самарская&quot;]">/rest/fias_clarify?address_json=["Рябиновый","Тольятти","Самарская"]</a></p>
<div class="ui existing segment">
<pre>
<div class="code">
{
  "parent": {
    "parent": {
      "parent": null,
      "ao_guid": "df3d7359-afa9-4aaa-8ff9-197e73906b1c",
      "formal_name": "Самарская",
      "short_name": "обл",
      "postal_code": 422624,
      "parent_guid": "8f252755-fdd4-44cd-93d0-05bd161939e7",
      "ao_level": 1
    },
    "ao_guid": "242e87c1-584d-4360-8c4c-aae2fe90048e",
    "formal_name": "Тольятти",
    "short_name": "г",
    "postal_code": 445091,
    "parent_guid": "df3d7359-afa9-4aaa-8ff9-197e73906b1c",
    "ao_level": 4
  },
  "ao_guid": "e21c96ae-86b8-49d4-91f0-fa299a737288",
  "formal_name": "Рябиновый",
  "short_name": "б-р",
  "postal_code": 445047,
  "parent_guid": "242e87c1-584d-4360-8c4c-aae2fe90048e",
  "ao_level": 7
}
      </div>
</pre>
</div>

<h4 class="ui header">Преобразование строки адреса в уникальный идентификатор ФИАС</h4>
<p>Поиск глобального идентификатора адресного объекта ФИАС по адресу, переданному в JSON</p>
<p>Вернет массив из одного глобального идентификатора в случае однозначности результата поиска, либо из нескольких</p>
<p>Элементы адреса в запросе должны быть в процессе увеличения (например, улица-город-регион)</p>
<p>Префиксы адресных единиц (улица, бульвар, пгт и пр.) в текущей реализации игнорируются</p>
<p><a href="/rest/fias_find?address_json=[&quot;Рябиновый&quot;,&quot;Тольятти&quot;,&quot;Самарская&quot;]">/rest/fias_find?address_json=["Рябиновый","Тольятти","Самарская"]</a></p>
<div class="ui existing segment">
<pre>
<div class="code">
["e21c96ae-86b8-49d4-91f0-fa299a737288"]
      </div>
</pre>
</div>

<h4 class="ui header">Поиск адресного объекта в ФИАС по идентификатору</h4>
<p>По глобальному идентификатору объекта ФИАС получаем данные о нём из базы данных</p>
<p>Адресные объекты включают в себя все адресные единицы уровнем от региона до улицы</p>
<p><a href="/rest/fias_ao?with_parents=false&ao_guid=413aac42-ea14-4010-9e4a-de90cfa2bd47">/rest/fias_ao?with_parents=false&ao_guid=413aac42-ea14-4010-9e4a-de90cfa2bd47</a></p>
<p><a href="/rest/fias_ao?ao_guid=413aac42-ea14-4010-9e4a-de90cfa2bd47">/rest/fias_ao?ao_guid=413aac42-ea14-4010-9e4a-de90cfa2bd47</a></p>
<div class="ui existing segment">
<pre>
<div class="code">
{
  "ao_guid":"413aac42-ea14-4010-9e4a-de90cfa2bd47",
  "formal_name":"Комсомольская",
  "short_name":"ул",
  "postal_code":"413320",
  "parent_guid":"85bc34b1-b0f1-4d0a-a376-a41d9167cb20",
  "ao_level":7
}
      </div>
</pre>
</div>
<h4 class="ui header">Поиск цепочки адресных объектов в ФИАС по идентификатору</h4>
<p>По глобальному идентификатору объекта ФИАС, например, по идентификатору улицы, получаем информацию о улице и о "вышестоящих" адресных объектах вплоть до региона</p>
<p><a href="/rest/fias_ao?with_parents=true&ao_guid=413aac42-ea14-4010-9e4a-de90cfa2bd47">/rest/fias_ao?with_parents=true&ao_guid=413aac42-ea14-4010-9e4a-de90cfa2bd47</a></p>
<div class="ui existing segment">
<pre>
<div class="code">
{
  "parent":{
    "parent":{
      "parent":{
        "parent":null,
        "ao_guid":"df594e0e-a935-4664-9d26-0bae13f904fe",
        "formal_name":"Саратовская",
        "short_name":"обл",
        "postal_code":410000,
        "parent_guid":"7644221b-0ef9-40c3-8378-cb0e2299747e",
        "ao_level":1
      },
      "ao_guid":"cf825a14-9b16-462c-a5bf-ddedf8353678",
      "formal_name":"Питерский",
      "short_name":"р-н",
      "postal_code":413233,
      "parent_guid":"df594e0e-a935-4664-9d26-0bae13f904fe",
      "ao_level":3
    },
    "ao_guid":"85bc34b1-b0f1-4d0a-a376-a41d9167cb20",
    "formal_name":"Питерка",
    "short_name":"с",
    "postal_code":413233,
    "parent_guid":"cf825a14-9b16-462c-a5bf-ddedf8353678",
    "ao_level":6
  },
  "ao_guid":"413aac42-ea14-4010-9e4a-de90cfa2bd47",
  "formal_name":"Комсомольская",
  "short_name":"ул",
  "postal_code":413320,
  "parent_guid":"85bc34b1-b0f1-4d0a-a376-a41d9167cb20",
  "ao_level":7
}
      </div>
</pre>
</div>

</@layout.basicLayout>