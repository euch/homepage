package org.ecsh.web.service;

import org.apache.commons.lang3.time.DateUtils;
import org.ecsh.homepage.Parsers;
import org.ecsh.web.domain.entity.FiasEventType;
import org.ecsh.web.domain.repository.FiasEventLogRepository;
import org.ecsh.web.domain.repository.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import scala.Option;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class MonitorService {

    @Autowired
    FiasEventLogRepository fiasEventLogRepository;

    @Autowired
    FiasService fiasService;

    public HashMap<String, Long> getEventLogStatisticsMap() {
        HashMap<String, Long> result = new HashMap<>();
        LocalDateTime horizon = LocalDateTime.now().minusMonths(1);
        result.put("s_" + FiasEventType.AO_BY_ADDRESS_UPREC,
                fiasEventLogRepository.countByTypeAndDate(FiasEventType.AO_BY_ADDRESS_UPREC, horizon, true));
        result.put("s_" + FiasEventType.ADDRESS_BY_AO_GUID_UPREC,
                fiasEventLogRepository.countByTypeAndDate(FiasEventType.ADDRESS_BY_AO_GUID_UPREC, horizon, true));
        result.put("s_" + FiasEventType.ADDRESS_BY_AO_GUID,
                fiasEventLogRepository.countByTypeAndDate(FiasEventType.ADDRESS_BY_AO_GUID, horizon, true));
        result.put("s_" + FiasEventType.AO_GUID_BY_ADDRESS,
                fiasEventLogRepository.countByTypeAndDate(FiasEventType.AO_GUID_BY_ADDRESS, horizon, true));
        result.put("f_" + FiasEventType.AO_BY_ADDRESS_UPREC,
                fiasEventLogRepository.countByTypeAndDate(FiasEventType.AO_BY_ADDRESS_UPREC, horizon, false));
        result.put("f_" + FiasEventType.ADDRESS_BY_AO_GUID_UPREC,
                fiasEventLogRepository.countByTypeAndDate(FiasEventType.ADDRESS_BY_AO_GUID_UPREC, horizon, false));
        result.put("f_" + FiasEventType.ADDRESS_BY_AO_GUID,
                fiasEventLogRepository.countByTypeAndDate(FiasEventType.ADDRESS_BY_AO_GUID, horizon, false));
        result.put("f_" + FiasEventType.AO_GUID_BY_ADDRESS,
                fiasEventLogRepository.countByTypeAndDate(FiasEventType.AO_GUID_BY_ADDRESS, horizon, false));
        result.put("x_" + FiasEventType.AO_BY_ADDRESS_UPREC,
                Optional.ofNullable(fiasEventLogRepository.execTimeByDateSec(FiasEventType.AO_BY_ADDRESS_UPREC, horizon)).orElse(0L));
        result.put("x_" + FiasEventType.ADDRESS_BY_AO_GUID_UPREC,
                Optional.ofNullable(fiasEventLogRepository.execTimeByDateSec(FiasEventType.ADDRESS_BY_AO_GUID_UPREC, horizon)).orElse(0L));
        result.put("x_" + FiasEventType.ADDRESS_BY_AO_GUID,
                Optional.ofNullable(fiasEventLogRepository.execTimeByDateSec(FiasEventType.ADDRESS_BY_AO_GUID, horizon)).orElse(0L));
        result.put("x_" + FiasEventType.AO_GUID_BY_ADDRESS,
                Optional.ofNullable(fiasEventLogRepository.execTimeByDateSec(FiasEventType.AO_GUID_BY_ADDRESS, horizon)).orElse(0L));

        return result;
    }

    public String sendFiasInfo() {
        return fiasService.isFiasEnabled() ? Parsers.wrapEventLogStatistics(getEventLogStatisticsMap()) : "";
    }

    @Value("#{'${openttd.server-name}'}") private String openttdServerName;
    public String sendOpenttdStatus() {
        Option<String> info = Parsers.getOpenTTDServerStatusCached(openttdServerName);
        return info.isDefined() ? info.get() : "";
    }

    public String sendOpenttdUrl() {
        Option<String> info = Parsers.getOpenTTDServerUrlCached(openttdServerName);
        return info.isDefined() ? info.get() : "";
    }

    @Value("#{'${news.twitter-rss.host}'}") private String twitter2rssHost;
    @Value("#{'${news.twitter-rss.port}'}") private Integer twitter2rssPort;
    public String sendTwitter2RssInfo() {
        return Parsers.isPortReachableCached(twitter2rssHost, twitter2rssPort) ? "OK" : "";
    }

    @Value("#{'${news.proxy.enable}'}") private Boolean proxyEnable;
    @Value("#{'${news.proxy.host}'}") private String proxyHost;
    @Value("#{'${news.proxy.port}'}") private Integer proxyPort;
    public String sendProxy1Info() {
        if (Parsers.isPortReachableCached(proxyHost, proxyPort))
            return (proxyEnable ? "OK, Enabled" : "OK, Disabled");
        else
            return "";
    }

    @Value("#{'${torrentbox.host}'}") private String torrentBoxHost;
    @Value("#{'${torrentbox.port}'}") private Integer torrentBoxPort;
    public String sendTorrentBoxStatus() {
        return Parsers.isPortReachableCached(torrentBoxHost, torrentBoxPort) ? "OK" : "";
    }
    public String sendTorrentBoxUrl() {
        return "http://"+torrentBoxHost+":"+torrentBoxPort;
    }

    @Autowired private NewsRepository newsRepository;
    public String sendCrawlerInfo() {
        Date now = new Date();
        Date dayBeggining = DateUtils.truncate(new Date(), Calendar.DATE);
        return String.format("Total: %s Today: %s",
                String.valueOf(newsRepository.count()),
                String.valueOf(newsRepository.findBySaveDateBetween(dayBeggining, now).size())
        );
    }


}
