package org.ecsh.web.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.ecsh.web.domain.dto.fias.AddrObj;
import org.ecsh.web.domain.entity.FiasEventLog;
import org.ecsh.web.domain.entity.FiasEventType;
import org.ecsh.web.domain.repository.FiasEventLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
public class FiasService {

    @Autowired
    private
    FiasEventLogRepository fiasEventLogRepository;

    @Value("#{'${fias.enabled}'}")
    private Boolean fiasEnabled;
    @Value("#{'${fias.datasource.db}'}")
    private String dbName;
    @Value("#{'${fias.datasource.server}'}")
    private String dbServer;
    @Value("#{'${fias.datasource.user}'}")
    private String dbUser;
    @Value("#{'${fias.datasource.pass}'}")
    private String dbPass;
    @Value("#{'${fias.datasource.driver}'}")
    private String dsDriver;

    private ObjectMapper om = new ObjectMapper();

    private HikariDataSource ds;

    @PostConstruct
    public void setDS() {
        if (fiasEnabled) {
            Properties props = new Properties();
            props.setProperty("dataSourceClassName", dsDriver);
            props.setProperty("dataSource.user", dbUser);
            props.setProperty("dataSource.password", dbPass);
            props.setProperty("dataSource.databaseName", dbName);
            props.setProperty("dataSource.serverName", dbServer);
            props.put("dataSource.logWriter", new PrintWriter(System.out));

            HikariConfig config = new HikariConfig(props);
            ds = new HikariDataSource(config);
        }
    }

    public Boolean isFiasEnabled() {
        return fiasEnabled;
    }

    public DeferredResult<String> processEvent(FiasEventType type, String params) {
        final DeferredResult<String> deferredResult = new DeferredResult<>();
        try {
            deferredResult.setResult(fiasEventLogRepository.findFreshResults(type, params, LocalDateTime.now().minusDays(1)).get(0).getResult());
        } catch (Exception obviously) {
            CompletableFuture.supplyAsync(() -> {
                FiasEventLog event = fiasEventLogRepository.save(new FiasEventLog(LocalDateTime.now(), type, params));
                switch (type) {
                    case AO_GUID_BY_ADDRESS:
                        event.setResult(guidsJsonByAddress(event.getParams()));
                        break;
                    case ADDRESS_BY_AO_GUID:
                        event.setResult(aoByGuid(event.getParams()));
                        break;
                    case ADDRESS_BY_AO_GUID_UPREC:
                        event.setResult(aoRecursiveJsonByGuid(event.getParams()));
                        break;
                    case AO_BY_ADDRESS_UPREC:
                        event.setResult(aoByAddress(event.getParams()));
                }
                if (event.getResult() != null) {
                    event.setFinishDate(LocalDateTime.now());
                    event.setSuccessful(true);
                    fiasEventLogRepository.save(event);
                }
                return event.getResult();
            }).thenApply(deferredResult::setResult);
        }
        return deferredResult;
    }

    private String aoByAddress(String addressJson) {
        List<String> guids = guidsByAddress(addressJson);
        return guids.size() == 1 ? aoRecursiveJsonByGuid(guids.get(0)) : "";
    }

    public String guidsJsonByAddress(String addressJson) {
        List<String> guids = guidsByAddress(addressJson);
        if (!guids.isEmpty()) try {
            StringWriter sw = new StringWriter();
            om.writeValue(sw, guids);
            return sw.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";

    }

    private List<String> guidsByAddress(String addressJson) {
        List<String> guids = new ArrayList<>();
        List<String> address = null;
        try {
            address = om.readValue(addressJson,
                    TypeFactory.defaultInstance().constructCollectionType(List.class,
                            String.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (address != null && !address.isEmpty()) {
            String placeholder = "/*nested*/";
            String subSQL = "SELECT distinct ao_guid from as_addrobj  where formal_name ilike ? " + placeholder;
            String selectSQL = subSQL;
            String inSubSql = "and parent_guid in (" + subSQL + ")";
            if (address.size() > 1) for (int i = 0; i < address.size() - 1; i++)
                selectSQL = selectSQL.replace(placeholder, inSubSql);
            try (Connection con = ds.getConnection()) {
                PreparedStatement ps = con.prepareStatement(selectSQL);
//                ps.closeOnCompletion();
                for (int i = 0; i < address.size(); i++) ps.setString(i + 1, address.get(i));
                ResultSet rs = ps.executeQuery();
                while (rs.next()) guids.add(rs.getString("ao_guid"));
//                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return guids;
    }

    public String aoRecursiveJsonByGuid(String aoGuid) {
        String json = "";
        try {
            AddrObj targetAo = aoByGuidDto(aoGuid);
            targetAo.findParentRecursively(this);
            json = om.writeValueAsString(targetAo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

    public AddrObj aoByGuidDto(String aoGuid) throws IOException {
        return om.readValue(aoByGuid(aoGuid), AddrObj.class);
    }

    public String aoByGuid(String aoGuid) {
        String result = "";
        String selectSQL = "select row_to_json(t) from(\n" +
                " select ao_guid, formal_name, short_name, postal_code, parent_guid, ao_level from as_addrobj  where live_status = true and ao_guid = ?\n" +
                " )as t";
        try (Connection con = ds.getConnection()) {
            PreparedStatement ps = con.prepareStatement(selectSQL);
//            ps.closeOnCompletion();
            ps.setObject(1, UUID.fromString(aoGuid));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) result = rs.getString("row_to_json");
//            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


}
