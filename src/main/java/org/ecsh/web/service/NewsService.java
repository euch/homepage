package org.ecsh.web.service;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import org.ecsh.web.domain.entity.NewsSource;
import org.ecsh.web.domain.entity.News;
import org.ecsh.web.domain.repository.NewsSourceRepository;
import org.ecsh.web.domain.repository.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URL;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;


@Service
public class NewsService {

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private NewsSourceRepository newsSourceRepository;

    private boolean watching = false;
    private int defaultTimeout = 100 * 1000; // 100 seconds

    @Value("#{'${news.proxy.enable}'}")
    private Boolean proxyEnabled;

    @Value("#{'${news.proxy.host}'}")
    private String proxyHost;

    @Value("#{'${news.proxy.port}'}")
    private String proxyPort;

    @PostConstruct
    public void init() {
        if (proxyEnabled) {
            System.setProperty("http.proxyHost", proxyHost);
            System.setProperty("http.proxyPort", proxyPort);
            System.setProperty("http.nonProxyHosts", twitter2rssHost); // |-separated
        }
    }

    @Scheduled(fixedRate = 10000)
    private void loop() {
        if (watching) return; //
        watching = true;
        List<NewsSource> newsSources = newsSourceRepository.findByEnabled(true);
        for (NewsSource newsSource : newsSources)
            try {
                newsSource.setPreparedURL(new URL(newsSource.getUrl().replace(twitter2rssNick, "http://" + twitter2rssHost + ":" + twitter2rssPort)));
                watchForNews(newsSource);
                Thread.sleep(defaultTimeout / newsSources.size());
            } catch (FeedException e) {
                System.out.println("NewsSource feed is not parseable: " + newsSource + " proxyEnabled=" + proxyEnabled);
            } catch (IOException e) {
                System.out.println("NewsSource feed is not available " + newsSource + " proxyEnabled=" + proxyEnabled);
            } catch (InterruptedException e) {
            }
        watching = false;
    }

    @Value("#{'${news.twitter-rss.nickname}'}")
    private String twitter2rssNick;

    @Value("#{'${news.twitter-rss.host}'}")
    private String twitter2rssHost;

    @Value("#{'${news.twitter-rss.port}'}")
    private Integer twitter2rssPort;

    public void watchForNews(NewsSource newsSource) throws IOException, FeedException {
        for (Object entry : new SyndFeedInput().build(new XmlReader(newsSource.getPreparedURL())).getEntries())
            processRecord((SyndEntry) entry, newsSource);
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        Calendar calUtcNow = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        newsSource.setLastUpdate(calUtcNow.getTime());
        newsSourceRepository.save(newsSource);
    }

    private void processRecord(SyndEntry entry, NewsSource newsSource) {

        ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
        Date saveDate = (Date.from(now.toInstant()));

        if (newsRepository.findByLink(entry.getLink()).size() == 0) {

            News record = new News();
            record.setTitle(entry.getTitle());
            record.setLink(entry.getLink());
            record.setSaveDate(saveDate);
            record.setPubDate(entry.getPublishedDate());
            record.setNewsSource(newsSource);
            if (newsSource.getContentMirroringEnabled()) record.setContent(entry.getDescription().getValue());
            try {
                newsRepository.save(record);
            } catch (Throwable th) {
                System.out.println("failed to save record from: " + entry.getLink());
                th.printStackTrace();
            }

        }

    }


}

