package org.ecsh.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Application {

    public static void main(String[] args) {
        Object[] cfg = new Object[]{MvcConfig.class, SecurityConfig.class};


        System.getProperties().setProperty("org.freemarker.jsp.metaInfTldSources", "classpath:spring-security-taglibs-3.2.7.RELEASE.jar$");


        SpringApplication.run(cfg, args);

//        Runtime.getRuntime().addShutdownHook();
    }
}
