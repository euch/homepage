package org.ecsh.web.security;

import org.ecsh.web.ContextLocator;
import org.ecsh.web.domain.entity.User;
import org.ecsh.web.domain.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SecurityUserDetailsService implements UserDetailsService {


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserRepository userRepository = ContextLocator.instance().getBean(UserRepository.class);
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Неверный логин или пароль");
        }
        return new SecurityUserDetails(user);
    }
}
