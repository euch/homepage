package org.ecsh.web;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ContextLocator implements ApplicationContextAware {


    private static ApplicationContext context;
    private static ContextLocator instance;

    private ContextLocator() {
    }

    public static ContextLocator instance() {
        if (instance == null) {
            instance = new ContextLocator();
        }
        return instance;
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    public <T> T getBean(Class<T> bean) {
        return context.getBean(bean);
    }

}
