package org.ecsh.web.domain.dto.fias;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.ecsh.web.service.FiasService;

import java.io.IOException;

public class AddrObj {

    @JsonProperty("ao_guid")
    String guid;

    @JsonProperty("formal_name")
    String name;

    @JsonProperty("short_name")
    String prefix;

    @JsonProperty("postal_code")
    Integer postalCode;

    @JsonProperty("parent_guid")
    String parentGuid;

    @JsonProperty("ao_level")
    Integer level;

    AddrObj parent;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public String getParentGuid() {
        return parentGuid;
    }

    public void setParentGuid(String parentGuid) {
        this.parentGuid = parentGuid;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public AddrObj getParent() {
        return parent;
    }

    public void findParentRecursively(FiasService fiasService) throws IOException {
        if (this.parentGuid != null && this.level != 1) {
            this.parent = fiasService.aoByGuidDto(parentGuid);
            if(this.parent != null) {
                this.parent.findParentRecursively(fiasService);
            }
        }
    }
}
