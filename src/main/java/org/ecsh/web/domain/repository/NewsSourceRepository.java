package org.ecsh.web.domain.repository;

import org.ecsh.web.domain.entity.NewsSource;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NewsSourceRepository extends CrudRepository<NewsSource, Long> {
    List<NewsSource> findByUrl(String url);

    List<NewsSource> findByEnabled(boolean b);
}
