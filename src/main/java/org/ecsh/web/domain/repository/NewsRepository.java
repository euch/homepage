package org.ecsh.web.domain.repository;

import org.ecsh.web.domain.entity.News;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface NewsRepository extends CrudRepository<News, Long> {
    List<News> findByTitleContainingIgnoreCaseOrderByIdDesc(String filter);
    List<News> findTop25ByOrderByIdDesc();
    List<News> findBySaveDateBetween(Date start, Date end);
    List<News> findByLink(String link);

}
