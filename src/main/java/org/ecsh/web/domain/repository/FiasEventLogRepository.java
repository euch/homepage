package org.ecsh.web.domain.repository;

import org.ecsh.web.domain.entity.FiasEventLog;
import org.ecsh.web.domain.entity.FiasEventType;
import org.ecsh.web.domain.entity.News;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface FiasEventLogRepository extends CrudRepository<FiasEventLog, Long> {
    @Query("select l from FiasEventLog l where l.type = ?1 and l.params = ?2 and l.addDate > ?3 and l.successful = true order by addDate desc")
    List<FiasEventLog> findFreshResults(FiasEventType type, String params, LocalDateTime horizon);

    @Query("select count(l) from FiasEventLog l where l.type = ?1 and l.addDate > ?2 and l.successful = ?3")
    Long countByTypeAndDate(FiasEventType type, LocalDateTime horizon, Boolean isSuccessful);

    @Query("select avg(extract(epoch from (l.finishDate-l.addDate))) from FiasEventLog l where l.type = ?1 and l.addDate > ?2 and l.successful = true")
    Long execTimeByDateSec(FiasEventType type, LocalDateTime horizon);
}