package org.ecsh.web.domain.entity;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class NewsSource {

    public NewsSource() {
    }

    public NewsSource(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public String toString() {
        return "URL: " + url + " Prepared URL: " + preparedURL;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank
    @Column(unique = true)
    private String url;

    @Transient
    private URL preparedURL;

    @Column(nullable = false)
    private Boolean enabled = true;

    @OneToMany(mappedBy = "newsSource")
    private List<News> news = new ArrayList<>();

    @Column(unique = true, nullable = false)
    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public List<News> getNews() {
        return news;
    }

    public void setNews(List<News> news) {
        this.news = news;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "mirror_content")
    private Boolean contentMirroringEnabled = true;

    public void setContentMirroringEnabled(Boolean v) {
        contentMirroringEnabled = v;
    }

    public Boolean getContentMirroringEnabled() {
        return contentMirroringEnabled;
    }

    public URL getPreparedURL() {
        return preparedURL;
    }

    public void setPreparedURL(URL preparedURL) {
        this.preparedURL = preparedURL;
    }
}