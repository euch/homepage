package org.ecsh.web.domain.entity;

import org.ecsh.web.LocalDateTimePersistenceConverter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
public class FiasEventLog {

    public FiasEventLog() {
    }

    public FiasEventLog(LocalDateTime addDate, FiasEventType type, String params) {
        this.addDate = addDate;
        this.type = type;
        this.params = params;
    }

    @Id()
    @GeneratedValue
    Long id;

    @Column(nullable = false)
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    private LocalDateTime addDate;

    @Convert(converter = LocalDateTimePersistenceConverter.class)
    private LocalDateTime finishDate;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private FiasEventType type;

    @NotNull
    private String params;

    @NotNull
    private Boolean successful = false;

    private String result;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getAddDate() {
        return addDate;
    }

    public void setAddDate(LocalDateTime addDate) {
        this.addDate = addDate;
    }

    public LocalDateTime getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(LocalDateTime finishDate) {
        this.finishDate = finishDate;
    }

    public FiasEventType getType() {
        return type;
    }

    public void setType(FiasEventType type) {
        this.type = type;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Boolean getSuccessful() {
        return successful;
    }

    public void setSuccessful(Boolean successful) {
        this.successful = successful;
    }
}
