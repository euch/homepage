package org.ecsh.web.domain.entity;

public enum FiasEventType { AO_GUID_BY_ADDRESS, ADDRESS_BY_AO_GUID, ADDRESS_BY_AO_GUID_UPREC, AO_BY_ADDRESS_UPREC}
