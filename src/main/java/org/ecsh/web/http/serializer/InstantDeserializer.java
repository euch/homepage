package org.ecsh.web.http.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.ecsh.web.MvcConfig;

import java.io.IOException;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;


public class InstantDeserializer extends JsonDeserializer<Instant> {
    @Override
    public Instant deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        String dateStr = jp.getValueAsString();
        DateTimeFormatter f = DateTimeFormatter.ofPattern(MvcConfig.dateTimeFormatPattern());
        return OffsetDateTime.parse(dateStr, f).atZoneSameInstant(ZoneId.of("UTC")).toInstant();
    }
}
