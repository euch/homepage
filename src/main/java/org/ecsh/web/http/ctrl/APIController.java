package org.ecsh.web.http.ctrl;

import org.apache.commons.lang3.time.DateUtils;
import org.ecsh.homepage.JsonUtils;
import org.ecsh.homepage.Parsers;
import org.ecsh.web.domain.entity.FiasEventType;
import org.ecsh.web.domain.entity.News;
import org.ecsh.web.domain.repository.NewsRepository;
import org.ecsh.web.service.FiasService;
import org.ecsh.web.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import scala.Option;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/rest/")
public class APIController {

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    NewsService newsService;

    @RequestMapping(value = "news/charts/sources_by_keywords", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    String sendChartData(@RequestParam(required = true) String keywords) {
        Map<String, Map<String, Long>> matchesByNewsSource = new HashMap<>();
        Arrays.asList(keywords.split(",")).stream().forEach(rawkw -> {
            String kw = rawkw.trim();
            if (!kw.isEmpty()) {
                List<News> newsList = newsRepository.findByTitleContainingIgnoreCaseOrderByIdDesc(kw);
                matchesByNewsSource.put(kw,
                        newsList.stream()
                                .collect(Collectors.groupingBy(e -> e.getNewsSource().getName(), Collectors.counting())));
            }
        });
        return JsonUtils.getChartJson(matchesByNewsSource);
    }

    @Autowired
    FiasService fiasService;

    @RequestMapping(value = "fias_ao", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    DeferredResult<String> sendFiasAddrobjInfo(
            @RequestParam(value = "ao_guid", required = true) String guid,
            @RequestParam(value = "with_parents", required = false, defaultValue = "false") Boolean withParents) {
        if (withParents) return fiasService.processEvent(FiasEventType.ADDRESS_BY_AO_GUID_UPREC, guid);
        else return fiasService.processEvent(FiasEventType.ADDRESS_BY_AO_GUID, guid);
    }

    @RequestMapping(value = "fias_find", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    DeferredResult<String> sendAddrobjGuid(
            @RequestParam(value = "address_json", required = true) String addressJson) {
        return fiasService.processEvent(FiasEventType.AO_GUID_BY_ADDRESS, addressJson);
    }

    @RequestMapping(value = "fias_clarify", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    DeferredResult<String> sendAddressClarification(
            @RequestParam(value = "address_json", required = true) String addressJson) {
        return fiasService.processEvent(FiasEventType.AO_BY_ADDRESS_UPREC, addressJson);
    }

    @RequestMapping(value = "openttd_server_date", method = RequestMethod.GET, produces = "text/plain")
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    String sendOpenttdServerDate(@RequestParam(value = "name") String serverName) {
        return Parsers.getOpenttdServerDate(serverName);
    }

}
