package org.ecsh.web.http.ctrl;

import org.ecsh.web.domain.repository.UserRepository;
import org.ecsh.web.service.MonitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.comparator.BooleanComparator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

@Controller
public class IndexController {

    @Value("#{'${openttd.server-name}'}")
    private String ttdServerName;

    @Value("#{'${fias.enabled}'}")
    private Boolean fiasEnabled;

    @Autowired
    private MonitorService monitorService;

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String getIndex(Model model) {
        model.addAttribute("ttdServerName", ttdServerName);
        model.addAttribute("ttdServerUrl", monitorService.sendOpenttdUrl());
        model.addAttribute("torrentboxUrl", monitorService.sendTorrentBoxUrl());
        model.addAttribute("fiasEnabled", fiasEnabled);

        return "index";
    }

    @RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
    public String getView(@PathVariable("id") String viewId, Model model, HttpServletResponse response) {
        String going = viewId;
        switch (viewId) {
            case "news":
                going = "aggregation/news";
                break;
            case "charts":
                going = "aggregation/charts";
                break;
            case "stats":
                model.addAttribute("ttdServerName", ttdServerName);
                model.addAttribute("ttdServerUrl", monitorService.sendOpenttdUrl());
                model.addAttribute("torrentboxUrl", monitorService.sendTorrentBoxUrl());
                break;
            case "fias":
                if (!fiasEnabled) going = "nih";
                break;
            default: going = "redirect:/404.html";
        }
        return going;
    }

}
