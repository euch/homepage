package org.ecsh.web.http.ctrl;

import org.ecsh.web.service.MonitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mon/")
public class MonitorController {

    @Autowired
    private MonitorService monitorService;

    @RequestMapping(value = "openttdInfo", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    String sendOpenttdInfo() {
        return monitorService.sendOpenttdStatus();
    }


    @RequestMapping(value = "fiasInfo", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    String sendFiasInfo() {
        return monitorService.sendFiasInfo();
    }


    @RequestMapping(value = "twitter2rssInfo", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    String sendTwitter2RssInfo() {
        return monitorService.sendTwitter2RssInfo();
    }


    @RequestMapping(value = "proxyInfo", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    String sendProxy1Info() {
       return monitorService.sendProxy1Info();
    }


    @RequestMapping(value = "torrentBoxInfo", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    String sendTorrentBoxInfo() {
        return monitorService.sendTorrentBoxStatus();
    }

    @RequestMapping(value = "crawlerInfo", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    String sendCrawlerInfo() {
       return monitorService.sendCrawlerInfo();
    }

}
