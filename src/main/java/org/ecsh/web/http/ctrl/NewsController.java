package org.ecsh.web.http.ctrl;

import org.ecsh.homepage.Parsers;
import org.ecsh.web.domain.entity.News;
import org.ecsh.web.domain.entity.NewsSource;
import org.ecsh.web.domain.repository.NewsRepository;
import org.ecsh.web.domain.repository.NewsSourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping(value = "news")
public class NewsController {

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private NewsSourceRepository newsSourceRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String sendNewsContent(@RequestParam(required = false) String filter, Model model) {
        boolean filtered = filter != null && filter.length() > 0;
        List<News> newsList = filtered ? newsRepository.findByTitleContainingIgnoreCaseOrderByIdDesc(filter) : newsRepository.findTop25ByOrderByIdDesc();
        model.addAttribute(newsList);
        return "aggregation/newsContent";
    }

    @Secured("ADMIN")
    @RequestMapping(method = RequestMethod.POST)
    public void addRssDirect(@RequestParam(required = true) String url) {
        addRss(url);
    }

    @Value("#{'${news.twitter-rss.nickname}'}")
    private String twitter2rssNick;

    @Secured("ADMIN")
    @RequestMapping(method = RequestMethod.POST, value = "/twi-user")
    public void addTwitterUser(@RequestParam(required = true) String username) {
        addRss(twitter2rssNick + "/user/" + username + ".xml");
    }

    @Secured("ADMIN")
    @RequestMapping(method = RequestMethod.POST, value = "/twi-tag")
    public void addTwitterTag(@RequestParam(required = true) String hashtag) {
        addRss(twitter2rssNick + "/htag/" + hashtag + ".xml");
    }

    private void addRss(String url) {
        List<NewsSource> existing = newsSourceRepository.findByUrl(url);
        if (existing.size() == 0)
            newsSourceRepository.save(new NewsSource(url, Parsers.getTitleByUrl(url)));
        else {
            existing.forEach(newsSource -> newsSource.setEnabled(true));
            newsSourceRepository.save(existing);
        }
    }

}