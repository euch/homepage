package org.ecsh.homepage

import scala.collection.mutable.ListBuffer

object JsonUtils {

// keyword -> source -> count
  def getChartJson(jdata: java.util.Map[String, java.util.Map[String,java.lang.Long]]) = {
    import scala.collection.JavaConversions._
    val data = jdata.toMap
    val sources = (data flatMap {_._2.toMap.keys}).toSet
    val jsonSources = "\"" + sources.mkString("\",\"") + "\""
    def jsonCountBySource(countBySource: Map[String, java.lang.Long]) = {
      val counts = ListBuffer[Long]()
      for(s <- sources) counts += countBySource.getOrDefault(s,0L)
      counts.mkString(",")
    }
    val rows = (for ((kw, countBySource) <- data) yield s"""["$kw",${jsonCountBySource(countBySource.toMap)},""]""").toList.mkString(",")
    s"""[["source",$jsonSources,{"role":"annotation"}],$rows]"""
  }

}
