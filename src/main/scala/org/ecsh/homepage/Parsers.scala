package org.ecsh.homepage

import java.net.{InetSocketAddress, Socket, URL}
import java.time.LocalDateTime

import org.apache.commons.lang3.StringEscapeUtils
import org.ecsh.web.domain.entity.FiasEventType
import org.htmlcleaner.{ContentNode, HtmlCleaner, TagNode}

import scala.collection.parallel.mutable


object Parsers {

  def wrapEventLogStatistics(data: java.util.Map[String, java.lang.Long]): String = {
    import FiasEventType._
    def compose(prefix: String, fiasEventType: FiasEventType, suffix: Option[String] = None) = "%s%s".format(data.get("%s_%s".format(prefix, fiasEventType)), if (suffix.isDefined) suffix.get else "")
    s"""<table>
        |  <th>
        |    <td>$ADDRESS_BY_AO_GUID</td>
        |    <td>$ADDRESS_BY_AO_GUID_UPREC</td>
        |    <td>$AO_BY_ADDRESS_UPREC</td>
        |    <td>$AO_GUID_BY_ADDRESS</td>
        |  </th>
        |  <tr>
        |    <td>succ. (M)</td>
        |    <td>${compose("s", ADDRESS_BY_AO_GUID)}</td>
        |    <td>${compose("s", ADDRESS_BY_AO_GUID_UPREC)}</td>
        |    <td>${compose("s", AO_BY_ADDRESS_UPREC)}</td>
        |    <td>${compose("s", AO_GUID_BY_ADDRESS)}</td>
        |  </tr>
        |    <tr>
        |    <td>avg. T (M)</td>
        |    <td>${compose("x", ADDRESS_BY_AO_GUID, Some("s"))}</td>
        |    <td>${compose("x", ADDRESS_BY_AO_GUID_UPREC, Some("s"))}</td>
        |    <td>${compose("x", AO_BY_ADDRESS_UPREC, Some("s"))}</td>
        |    <td>${compose("x", AO_GUID_BY_ADDRESS, Some("s"))}</td>
        |  </tr>
        |  <tr>
        |    <td>fail (M)</td>
        |    <td>${compose("f", ADDRESS_BY_AO_GUID)}</td>
        |    <td>${compose("f", ADDRESS_BY_AO_GUID_UPREC)}</td>
        |    <td>${compose("f", AO_BY_ADDRESS_UPREC)}</td>
        |    <td>${compose("f", AO_GUID_BY_ADDRESS)}</td>
        |  </tr>
        |</table>
    """.stripMargin
  }

  def getTitleByUrl(url: String): String = {
    val pageHtml = scala.xml.XML.loadString(scala.io.Source.fromURL(url).mkString)
    val titleTag: String = (pageHtml \ "channel" \ "title").toString()
    """>.*<""".r.findFirstMatchIn(titleTag).get.toString().replace(">", "").replace("<", "")
  }

  def getOpenTTDServerStatusCached(serverName: String) = getOpenttdAvailabilityCached(serverName) match {
    case None => None
    case Some(info) => info.status
  }

  def getOpenTTDServerUrlCached(serverName: String) = getOpenttdAvailabilityCached(serverName) match {
    case None => None
    case Some(info) => info.url
  }

  def getOpenttdAvailabilityCached(serverName: String): Option[OpenttdAvailableOnTime] = {
    val cacheKey = OpenTTDServerInfo(serverName)
    this.synchronized {
      availabilityCache.get(cacheKey) match {
        case Some(expired) if expired.time.plusMinutes(1).isBefore(now) => availabilityCache.update(cacheKey, getOpenttdAvailability(cacheKey))
        case None => availabilityCache.put(cacheKey, getOpenttdAvailability(cacheKey))
        case Some(actual) =>
      }
      availabilityCache.get(cacheKey)
    }.asInstanceOf[Option[OpenttdAvailableOnTime]]
  }

  def isPortReachableCached(host: String, port: Int): Boolean = {
    val cacheKey = HostPort(host, port)
    this.synchronized {
      availabilityCache.get(cacheKey) match {
        case Some(expired) if expired.time.plusMinutes(1).isBefore(now) => availabilityCache.update(cacheKey, isPortReachable(cacheKey))
        case None => availabilityCache.put(cacheKey, isPortReachable(cacheKey))
        case Some(actual) =>
      }
      availabilityCache.get(cacheKey) match {
        case Some(cached) => cached.available
        case None => false
      }
    }
  }

  private def getOpenttdAvailability(openTTDServerInfo: OpenTTDServerInfo): OpenttdAvailableOnTime = {
    val cleaner = new HtmlCleaner

    def getServerPage(serverName: String): Option[String] = try {
      cleaner.clean(new URL("https://www.openttd.org/en/servers")).getElementsByName("tr", true)
        .filter(elem => StringEscapeUtils.unescapeHtml4(elem.getText.toString).contains(openTTDServerInfo.serverName))
        .map(elem => "https://www.openttd.org/" + elem.getAllChildren.get(3).asInstanceOf[TagNode].getAllChildren.get(0).asInstanceOf[TagNode].getAllChildren.get(0).asInstanceOf[TagNode].getAttributeByName("href"))
        .headOption
    } catch {
      case th: Throwable =>
        println(s"failed to get openttd server page ${th.getMessage}")
        th.printStackTrace()
        None
    }

    getServerPage(openTTDServerInfo.serverName) match {
      case None =>
        return OpenttdAvailableOnTime(available = false)

      case Some(sp) =>
        val elements = cleaner.clean(new URL(sp)).getElementsByName("tr", true).toList

        def getRowByName(name: String): Option[String] = try {
          elements.filter(_.getAllChildren.get(0) match {
            case tn: TagNode =>
              tn.getAllChildren.get(0) match {
                case cn: ContentNode => cn.getContent.equals(name)
                case not => false
              }
            case not => false
          }).map(_.getAllChildren.get(1).asInstanceOf[TagNode].getText.toString).headOption
        } catch {
          case th: Throwable => None
        }

        val gameDate = getRowByName("Current date:")
        val clients = getRowByName("Clients:")
        val status = Some(s"""Clients: ${clients.getOrElse("n/a")}<br/>Date: ${gameDate.getOrElse("n/a")}""")
        return OpenttdAvailableOnTime(available = true, status = status, url = Some(sp), gameDate = gameDate)
    }


    OpenttdAvailableOnTime(available = false)
  }

  def getOpenttdServerDate(serverName: String): String = {
    getOpenttdAvailabilityCached(serverName) match {
      case None => ""
      case Some(s) => s.gameDate match {
        case None => ""
        case Some(d) => d
      }
    }
  }

  private def now = LocalDateTime.now()


  private val availabilityCache = mutable.ParHashMap[CacheKey, AvailabilityOnTime]()

  private trait CacheKey

  private case class OpenTTDServerInfo(serverName: String) extends CacheKey

  private case class HostPort(host: String, port: Int) extends CacheKey

  trait AvailabilityOnTime {
    val available: Boolean
    val time = now
    val status: Option[String]
    val url: Option[String]
  }

  case class HostAvailableOnTime(
                              available: Boolean,
                              status: Option[String] = None,
                              url: Option[String] = None
                            ) extends AvailabilityOnTime

  case class OpenttdAvailableOnTime(
                                     available: Boolean,
                                     status: Option[String] = None,
                                     url: Option[String] = None,
                                     gameDate: Option[String] = None
                                   ) extends AvailabilityOnTime


  private def isPortReachable(address: HostPort): HostAvailableOnTime = try {
    val socket = new Socket()
    socket.connect(new InetSocketAddress(address.host, address.port), 5000)
    socket.close()
    HostAvailableOnTime(available = true)
  } catch {
    case ignored: Throwable =>
      HostAvailableOnTime(available = false)
  }

}
