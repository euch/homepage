import org.ecsh.web.Application;
import org.ecsh.web.domain.entity.FiasEventType;
import org.ecsh.web.domain.repository.FiasEventLogRepository;
import org.ecsh.web.service.MonitorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebIntegrationTest
public class MonTest {

    @Autowired
    MonitorService monitorService;

   @Test
    public void fiasEventLogCountTest() {
        HashMap<String, Long> result = monitorService.getEventLogStatisticsMap();
        result.forEach((k, v) -> System.out.println(k+"="+v));
        assert(result.values().stream().filter(v -> v>0).toArray().length > 0);

    }


    @Test
    public void fiasFormattingTest() {
        System.out.printf(monitorService.sendFiasInfo());
    }



}