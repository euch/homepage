/*
import com.fasterxml.jackson.databind.ObjectMapper;
import org.ecsh.web.Application;
import org.ecsh.web.domain.dto.fias.AddrObj;
import org.ecsh.web.service.FiasService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebIntegrationTest
public class FiasTest {

    @Autowired
    FiasService fiasService;

    @Test
    public void getAoByGUIDTest() throws IOException {
        String json = fiasService.aoByGuid("413aac42-ea14-4010-9e4a-de90cfa2bd47");
        assert (json != null);
           String result = new ObjectMapper().readValue(json, AddrObj.class).getName();
        assert ("Комсомольская".equals(result));
    }

    @Test
    public void aoRecursiveByGuidTest() throws IOException {
        String json = fiasService.aoRecursiveJsonByGuid("413aac42-ea14-4010-9e4a-de90cfa2bd47");
        System.out.printf(json);
        AddrObj result = new ObjectMapper().readValue(json, AddrObj.class);
        assert ("Комсомольская".equals(result.getName()));
        assert ("Питерка").equals(result.getParent().getName());
    }

    @Test
    public void aoFromGuidTest() {
        assert (fiasService.guidsJsonByAddress("[\"Рябиновый\", \"Тольятти\", \"Самарская\"]")).equals("[\"e21c96ae-86b8-49d4-91f0-fa299a737288\"]");
    }



}*/
